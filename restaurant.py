from menu_item import MenuItem


class Restaurant:
    def __init__(self, name, cuisine, address, menu):
        self.menu = menu
        self.address = address
        self.cuisine = cuisine
        self.name = name

    def add_menu_item(self, name, price):
        menu_item = MenuItem(name, price)
        self.menu.append(menu_item)

    def set_address(self, address):
        self.address = address




