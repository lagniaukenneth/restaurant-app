class MenuItem:
    def __init__(self, name, price):
        self.price = price
        self.name = name

    def set_price(self, price):
        if price < 0:
            raise Exception("Your price is below 0")
        else:
            self.price = price

