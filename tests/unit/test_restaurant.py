from unittest import TestCase

from restaurant import Restaurant


class TestRestaurant(TestCase):
    def test_set_address(self):

        dummy_resto = Restaurant(
            "test",
            "test",
            "test",
            []
        )

        actual_address = dummy_resto.address
        expected_address = "test"
        self.assertEqual(expected_address, actual_address)
