from unittest import TestCase

from restaurant import Restaurant


class TestRestaurant(TestCase):
    def test_add_item(self):
        resto_dummy = Restaurant(
            "test",
            "test",
            "test",
            []
        )
        item_name = "Water"
        item_price = 2

        resto_dummy.add_menu_item(item_name, item_price)

        self.assertEqual(1, len(resto_dummy.menu))
        self.assertEqual("Water", resto_dummy.menu[0].name)
        self.assertEqual(2, resto_dummy.menu[0].price)



